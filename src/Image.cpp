#include "Image.h"
#include <iostream>
#include <string>
#include <cassert>
#include <fstream>


using namespace std;

/**
 * @brief Constructeur par défaut, initialise dimx et dimy à 0
 */
Image::Image()
{
    tab = nullptr;
    dimx = 0, dimy = 0;
    m_window = nullptr;
    m_surface = nullptr;
    m_renderer = nullptr;
    m_texture = nullptr;
    m_hasChanged = false;
}


Image::~Image()
{
    if (tab != nullptr) {
        delete[] tab;
        tab = nullptr;
    }
    dimx=0, dimy=0;
    if (m_window != nullptr) {
        SDL_FreeSurface(m_surface);
        SDL_DestroyTexture(m_texture);
        m_surface = nullptr;
        m_texture = nullptr;
        m_hasChanged = false;
        SDL_DestroyRenderer(m_renderer);
        SDL_DestroyWindow(m_window);
        SDL_Quit();
    }
}

/**
 * @brief Constructeur par copie
 * @param dimensionX
 * @param dimensionY
 */

Image::Image(unsigned int dimensionX, unsigned int dimensionY)
{
    if (dimensionX > 0)
    {
        dimx = dimensionX;
    }
    else
    {
        dimx = 0;
    }
    if (dimensionY > 0)
    {
        dimy = dimensionY;
    }
    else
    {
        dimy = 0;
    }
    tab = new Pixel[dimx*dimy];
    for (unsigned int i=0; i<dimx*dimy; i++)
    {
        tab[i] = Pixel(0,0,0);
    }
    m_window = nullptr;
    m_surface = nullptr;
    m_renderer = nullptr;
    m_texture = nullptr;
    m_hasChanged = false;
}

/**
 * @brief Récupère et renvoie l'adresse du pixel aux coordonnées (x,y)
 * @param x
 * @param y
 * @return Adresse du pixel
 */
Pixel& Image::getPix(unsigned int x, unsigned int y) const
{
    if (x >= 0 && x < dimx && y >= 0 && y < dimy)
    {
        return tab[y*dimx+x];
    }
    return tab[0];
}

/**
 * @brief Change la couleur du pixel aux coordonnées (x,y)
 * @param x
 * @param y
 * @param couleur
 */

void Image::setPix(unsigned int x, unsigned int y, const Pixel& couleur)
{
    if (x >= 0 && x < dimx && y >= 0 && y < dimy)
    {
        tab[y*dimx+x].setRouge(couleur.getRouge());
        tab[y*dimx+x].setVert(couleur.getVert());
        tab[y*dimx+x].setBleu(couleur.getBleu());
    }
}

/**
 * @brief Dessine un rectangle sur l'image
 * @param Xmin
 * @param Ymin
 * @param Xmax
 * @param Ymax
 * @param couleur
 */

void Image::dessinerRectangle(unsigned int Xmin, unsigned int Ymin, unsigned int Xmax, unsigned int Ymax, const Pixel& couleur)
{
    if (Xmin >= 0 && Ymin >= 0 && Xmax < dimx && Ymax < dimy)
    {
        for (unsigned int i=Xmin; i<=Xmax; i++)
        {
            for (unsigned int j=Ymin; j<=Ymax; j++)
            {
                setPix(i, j, couleur);
            }
        }
    }
}

/**
 * @brief Remplit toute l'image d'une couleur
 * @param couleur
 */
void Image::effacer(const Pixel& couleur)
{
    dessinerRectangle(0,0,dimx-1, dimy-1, couleur);
}

/**
 * @brief Effectue les tests de régression
 */
void Image::testRegression()
{
    Image im(5,5);
    Pixel p(123,123,123);
    assert(p.getRouge() == 123);
    assert(p.getVert() == 123);
    assert(p.getBleu() == 123);
    Pixel p1;
    assert(p1.getRouge() == 0);
    assert(p1.getVert() == 0);
    assert(p1.getBleu() == 0);
    im.dessinerRectangle(2,2,4,4, p);
    im.afficherConsole();
    im.effacer(p1);
    im.afficherConsole();
    im.setPix(0,0,p);
    im.afficherConsole();
}


/**
 * @brief Sauvegarde l'image dans un fichier
 * @param filename
 */
void Image::sauver(const string & filename) const
{
    ofstream fichier(filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << endl;
    fichier << dimx << " " << dimy << endl;
    fichier << "255" << endl;
    for(unsigned int y=0; y<dimy; ++y)
    {
        for(unsigned int x=0; x<dimx; ++x)
        {
            Pixel pix = getPix(x,y);
            fichier << (int) pix.getRouge() << " " << (int) pix.getVert() << " " << (int) pix.getBleu() << " ";
        }
        fichier << endl;
    }
    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

/**
 * @brief Ouvre l'image contenue dans un fichier
 * @param filename
 */
void Image::ouvrir(const string & filename)
{
    ifstream fichier (filename.c_str());
    assert(fichier.is_open());
    int r,g,b;
    string mot;
    dimx = dimy = 0;
    fichier >> mot >> dimx >> dimy >> mot;
    assert(dimx > 0 && dimy > 0);
    if (tab != nullptr) delete[] tab;
    tab = new Pixel [dimx*dimy];
    for(unsigned int y=0; y<dimy; ++y)
        for(unsigned int x=0; x<dimx; ++x)
        {
            fichier >> r >> g >> b;
            getPix(x,y).setRouge(r);
            getPix(x,y).setVert(g);
            getPix(x,y).setBleu(b);
        }
    fichier.close();
    cout << "Lecture de l'image " << filename << " ... OK\n";
}

/**
 * @brief Affiche l'image sous forme textuelle dans la console
 */
void Image::afficherConsole() const
{
    cout << dimx << " " << dimy << endl;
    for(unsigned int y=0; y<dimy; ++y)
    {
        for(unsigned int x=0; x<dimx; ++x)
        {
            Pixel pix = getPix(x,y);
            cout << (int) pix.getRouge() << " " << (int) pix.getVert() << " " << (int) pix.getBleu() << " ";
        }
        cout << endl;
    }
}

    /**
     * @brief Affiche l'image sur une fenêtre 200x200
     */
void Image::afficher() {

    initAffichage();
    const char* filename = "data/temp.ppm";
    sauver(filename);

    m_surface = IMG_Load(filename);
    if (m_surface == nullptr) {
        string nfn = string("../") + filename;
        cout << "Error: cannot load "<< filename <<". Trying "<<nfn<<endl;
        m_surface = IMG_Load(nfn.c_str());
        if (m_surface == nullptr) {
            nfn = string("../") + nfn;
            m_surface = IMG_Load(nfn.c_str());
        }
    }
    if (m_surface == nullptr) {
        cout<<"Error: cannot load "<< filename <<endl;
        exit(1);
    }

    SDL_Surface * surfaceCorrectPixelFormat = SDL_ConvertSurfaceFormat(m_surface,SDL_PIXELFORMAT_ARGB8888,0);
    SDL_FreeSurface(m_surface);
    m_surface = surfaceCorrectPixelFormat;

    m_texture = SDL_CreateTextureFromSurface(m_renderer,surfaceCorrectPixelFormat);
    if (m_texture == nullptr) {
        cout << "Error: problem to create the texture of "<< filename<< endl;
        exit(1);
    }
    bouclePrincipale();
}

void Image::initAffichage() {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << endl;SDL_Quit();exit(1);
    }

    int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
    if( !(IMG_Init(imgFlags) & imgFlags)) {
        cout << "SDL_m_image could not initialize! SDL_m_image Error: " << IMG_GetError() << endl;SDL_Quit();exit(1);
    }

    int fen_x, fen_y;
    fen_x = 200;
    fen_y = 200;

    m_window = SDL_CreateWindow("Module Image", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, fen_x, fen_y, SDL_WINDOW_SHOWN);
    if (m_window == nullptr) {
        cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << endl; SDL_Quit(); exit(1);
    }

    m_renderer = SDL_CreateRenderer(m_window,-1,SDL_RENDERER_ACCELERATED);
}
void Image::bouclePrincipale() {
    SDL_Event events;
    bool quit = false;
    int zoom = 2;
    while (!quit) {

        // tant qu'il y a des evenements à traiter (cette boucle n'est pas bloquante)
        while (SDL_PollEvent(&events)) {
            if (events.type == SDL_QUIT) quit = true;           // Si l'utilisateur a clique sur la croix de fermeture
            else if (events.type == SDL_KEYDOWN)
            {
                switch (events.key.keysym.scancode)
                {
                    case SDL_SCANCODE_SPACE:
                        quit = true;
                        break;
                    case SDL_SCANCODE_T:
                        if (zoom > 1) zoom -= 1;
                        break;
                    case SDL_SCANCODE_G:
                        if (zoom < 10) zoom += 1;
                        break;
                    default: break;
                }
            }
        }
        SDL_SetRenderDrawColor(m_renderer, 226, 226, 226, 255);
        SDL_RenderClear(m_renderer);
        draw(m_renderer, 50-50*(zoom-1), 50-50*(zoom-1), 100*zoom, 100*zoom);
        // on permute les deux buffers (cette fonction ne doit se faire qu'une seule fois dans la boucle)
        SDL_RenderPresent(m_renderer);
    }
}
/**
 * @brief Dessine la texture
 * @param m_renderer
 * @param x
 * @param y
 * @param w
 * @param h
 */
void Image::draw (SDL_Renderer * m_renderer, int x, int y, int w, int h) {
    int ok;
    SDL_Rect r;
    r.x = x;
    r.y = y;
    r.w = (w<0)?m_surface->w:w;
    r.h = (h<0)?m_surface->h:h;

    if (m_hasChanged) {
        ok = SDL_UpdateTexture(m_texture,nullptr,m_surface->pixels,m_surface->pitch);
        assert(ok == 0);
        m_hasChanged = false;
    }

    ok = SDL_RenderCopy(m_renderer,m_texture,nullptr,&r);
    assert(ok == 0);
}



