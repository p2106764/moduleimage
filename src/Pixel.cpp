#include "Pixel.h"
using namespace std;

/**
 * @brief Constructeur par défaut
 */
Pixel::Pixel() : r(0), g(0), b(0) {}

/**
 * @brief Constructeur par copie, initialise la couleur du pixel avec les valeurs en paramètre
 * @param nr Quantité de rouge (0 à 255)
 * @param ng Quantité de vert (0 à 255)
 * @param nb Quantité de bleu (0 à 255)
 */
Pixel::Pixel(unsigned char nr, unsigned char ng, unsigned char nb)
{
    (nr >= 0 && nr <= 255) ? r = nr : r = 0;
    (ng >= 0 && ng <= 255) ? g = ng : g = 0;
    (nb >= 0 && nb <= 255) ? b = nb : b = 0;
}

/**
 *
 * @return Retourne la quantité de rouge
 */
unsigned char Pixel::getRouge() const
{
    return r;
}
/**
 * @return Retourne la quantité de vert
 */
unsigned char Pixel::getVert() const
{
    return g;
}
/**
 * @return Retourne la quantité de bleu
 */
unsigned char Pixel::getBleu() const
{
    return b;
}

/**
 * @brief Change la quantité de rouge du pixel
 * @param nr Quantité de rouge (0 à 255)
 */
void Pixel::setRouge(unsigned char nr)
{
    if (nr >= 0 && nr <= 255)
    {
        r = nr;
    }
}

/**
 * @brief Change la quantité de vert du pixel
 * @param ng Quantité de vert (0 à 255)
 */
void Pixel::setVert(unsigned char ng)
{
    if (ng >= 0 && ng <= 255)
    {
        g = ng;
    }
}

/**
 * @brief Change la quantité de bleu du pixel
 * @param nb Quantité de bleu (0 à 255)
 */
void Pixel::setBleu(unsigned char nb)
{
    if (nb >= 0 && nb <= 255)
    {
        b = nb;
    }
}


