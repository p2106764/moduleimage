#ifndef PIXEL_H_INCLUDED
#define PIXEL_H_INCLUDED
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

using namespace std;

class Pixel
{
private:
    unsigned char r, g, b;

public:
        /**
     * @brief Constructeur par défaut
     */
    Pixel();
        /**
     * @brief Constructeur par copie, initialise la couleur du pixel avec les valeurs en paramètre
     * @param nr Quantité de rouge (0 à 255)
     * @param ng Quantité de vert (0 à 255)
     * @param nb Quantité de bleu (0 à 255)
     */
    Pixel(unsigned char nr, unsigned char ng, unsigned char nb);
        /**
     * @return Retourne la quantité de rouge
     */
    unsigned char getRouge() const;
        /**
     * @return Retourne la quantité de vert
     */
    unsigned char getVert() const;
        /**
     * @return Retourne la quantité de bleu
     */
    unsigned char getBleu() const;
        /**
     * @brief Change la quantité de rouge du pixel
     * @param nr Quantité de rouge (0 à 255)
     */
    void setRouge(unsigned char nr);
        /**
     * @brief Change la quantité de vert du pixel
     * @param ng Quantité de vert (0 à 255)
     */
    void setVert(unsigned char ng);
        /**
     * @brief Change la quantité de bleu du pixel
     * @param nb Quantité de bleu (0 à 255)
     */
    void setBleu(unsigned char nb);


};

#endif // PIXEL_H_INCLUDED
