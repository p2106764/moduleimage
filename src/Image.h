#ifndef IMAGE_H_INCLUDED
#define IMAGE_H_INCLUDED
#include "Pixel.h"
#include <vector>
#include <string>


using namespace std;

class Image
{
private:
    Pixel * tab;
    unsigned int dimx, dimy;
    SDL_Window * m_window;
    SDL_Renderer * m_renderer;
    SDL_Surface * m_surface;
    SDL_Texture * m_texture;
    bool m_hasChanged;
        /**
     * @brief Dessine la texture
     * @param m_renderer
     * @param x
     * @param y
     * @param w
     * @param h
     */
    void draw(SDL_Renderer * m_renderer, int x, int y, int w, int h);
        /**
     * @brief Boucle principale SDL
     */
    void bouclePrincipale();

        /**
     * @brief Initialise la fenêtre SDL
     */
    void initAffichage();

public:
        /**
     * @brief Constructeur par défaut, initialise dimx et dimy à 0
     */
    Image();
        /**
     * @brief Constructeur par copie
     * @param dimensionX
     * @param dimensionY
     */
    Image(unsigned int dimensionX, unsigned int dimensionY);
    /**
     * @brief Destructeur Image
     */
    ~Image();
        /**
     * @brief Récupère et renvoie l'adresse du pixel aux coordonnées (x,y)
     * @param x
     * @param y
     * @return Adresse du pixel
     */
    Pixel& getPix(unsigned int x,unsigned int y) const;
        /**
     * @brief Change la couleur du pixel aux coordonnées (x,y)
     * @param x
     * @param y
     * @param couleur
     */
    void setPix(unsigned int x, unsigned int y, const Pixel& couleur);
        /**
     * @brief Dessine un rectangle sur l'image
     * @param Xmin
     * @param Ymin
     * @param Xmax
     * @param Ymax
     * @param couleur
     */
    void dessinerRectangle(unsigned int Xmin, unsigned int Ymin,unsigned int Xmax, unsigned int Ymax, const Pixel& couleur);
        /**
    * @brief Remplit toute l'image d'une couleur
    * @param couleur
    */
    void effacer(const Pixel& couleur);
        /**
     * @brief Effectue les tests de régression
     */
    void testRegression();
        /**
     * @brief Sauvegarde l'image dans un fichier
     * @param filename
     */
    void sauver(const string & filename) const;
        /**
     * @brief Ouvre l'image contenue dans un fichier
     * @param filename
     */
    void ouvrir(const string & filename);
        /**
     * @brief Affiche l'image sous forme textuelle dans la console
     */
    void afficherConsole() const;
    /**
     * @brief Affiche l'image sur une fenêtre 200x200
     */
    void afficher ();

};

#endif // IMAGE_H_INCLUDED
