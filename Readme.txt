Module Image

P2106764 AHRIOUI Younes

https://forge.univ-lyon1.fr/p2106764/moduleimage
-----------------------
DESCRIPTION

Le module Image permet de créer des images au format ppm,
les afficher en version textuelle ou sur une fenêtre SDL,
les sauvegarder et les récupérer depuis un fichier.

Il y a 3 executables :
"test" qui lance le test de régression de la classe Image.

"exemple" qui teste les fonctionnalités de sauvegarde et récuperation d'une image depuis un fichier.

"affichage" qui permet d'afficher une image sur une fenêtre SDL, appuyer sur T pour dézoomer et G pour zoomer sur l'image.

-------------------------------------------------------------------------

DEPENDANCES

SDL2

-------------------------------------------------------------------------

ORGANISATION DE L'ARCHIVE (après compilation et execution des programmes)


P2106764/
	bin/
		exemple
		test
		affichage
	data/
		image1.ppm
		image2.ppm
		temp.ppm
	doc/
		html/
			...
			...
			index.html
		image.doxy
	
	obj/
		Image.o
		Pixel.o
		mainExemple.o
		mainTest.o
		mainAffichage.o
	
	src/
		Image.cpp
		Pixel.cpp
		mainExemple.cpp
		mainTest.cpp
		mainAffichage.cpp
		Image.h
		Pixel.h
	
	Makefile
	Readme.txt
	
--------------------------------------------------------------------------

COMPILATION

Les commandes sont à lancer dans le répertoire P2106764 de l'archive

Pour compiler les 3 executables, lancer la commande : make
Pour compiler l'executable exemple, lancer la commande : make bin/exemple
Pour compiler l'executable test, lancer la commande : make bin/test
Pour compiler l'executable affichage, lancer la commande : make bin/affichage

--------------------------------------------------------------------------

EXECUTION (à faire après avoir compilé)

Les commandes sont à lancer dans le répertoire P2106764 de l'archive

Pour executer "exemple", lancer la commande : ./bin/exemple
Pour executer "test", lancer la commande : ./bin/test
Pour executer "affichage", lancer la commande : ./bin/affichage



