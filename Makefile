CC = g++
all: bin/exemple bin/test bin/affichage

bin/exemple: obj/Image.o obj/Pixel.o obj/mainExemple.o
	$(CC) obj/mainExemple.o obj/Image.o obj/Pixel.o -o bin/exemple -lSDL2 -lSDL2_image
	
bin/test: obj/Image.o obj/Pixel.o obj/mainTest.o
	$(CC) obj/mainTest.o obj/Image.o obj/Pixel.o -o bin/test -lSDL2 -lSDL2_image

bin/affichage: obj/Image.o obj/Pixel.o obj/mainAffichage.o
	$(CC) obj/mainAffichage.o obj/Image.o obj/Pixel.o -o bin/affichage -lSDL2 -lSDL2_image
	
obj/mainExemple.o: src/mainExemple.cpp src/Image.h src/Pixel.h
	$(CC) -c src/mainExemple.cpp -o obj/mainExemple.o -lSDL2 -lSDL2_image

obj/mainTest.o: src/mainTest.cpp src/Image.h src/Pixel.h
	$(CC) -c src/mainTest.cpp -o obj/mainTest.o -lSDL2 -lSDL2_image

obj/mainAffichage.o: src/mainAffichage.cpp src/Image.h src/Pixel.h
	$(CC) -c src/mainAffichage.cpp -o obj/mainAffichage.o -lSDL2 -lSDL2_image
	
obj/Image.o: src/Image.cpp src/Image.h src/Pixel.h
	$(CC) -c src/Image.cpp -o obj/Image.o

obj/Pixel.o: src/Pixel.cpp src/Pixel.h
	$(CC) -c src/Pixel.cpp -o obj/Pixel.o

clean:
	rm -f -r obj/*
	rm -f data/*
	rm -f bin/*
